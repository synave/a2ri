/*************************************/
/* Auteur : Rémi Synave              */
/* Date de création : 23/02/10       */
/* Date de modification : 25/02/10   */
/* Version : 0.4                     */
/*************************************/

/***************************************************************************/
/* This file is part of a2ri.                                              */
/*                                                                         */
/* a2ri is free software: you can redistribute it and/or modify it         */
/* under the terms of the GNU Lesser General Public License as published   */
/* by the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                     */
/*                                                                         */
/* a2ri is distributed in the hope that it will be useful,                 */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/* GNU Lesser General Public License for more details.                     */
/*                                                                         */
/* You should have received a copy of the GNU Lesser General Public        */
/* License along with a2ri.                                                */
/* If not, see <http://www.gnu.org/licenses/>.                             */
/***************************************************************************/



#ifndef CONFIG__H
#define CONFIG__H

#define A2RI_PRECISION_FLOAT 1e-10

#ifndef PI
#define PI 3.14159265359
#endif

#define A2RI_NB_THREAD 1

#define GEODESIC_DENSIFICATION_NB_SUBDIV 1

#endif
