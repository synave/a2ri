# a2ri

Bibliothèque de manipulation de maillage triangulaire.
Le site web original est ici : liba2ri.free.fr mais je n'ai plus la main dessus.

Le but de cette bibliothèque est le recalage, la reconstruction et le traitement de surfaces dans le cadre d’une chaîne de traitement complète, de l'acquisition à l'impression 3D. Les principales fonctionnalités sont donc :
- manipulation de maillages triangulaires
- recalage de maillages triangulaires
- reconstruction de surface à partir des vues obtenues par acquisition au scanner laser
- traitement d’une surface non imprimable en vue d'une impression

Auteur : Rémi Synave
email : remi.synave@nuiv-littoral.fr

Ce code est toujours en développement. N'hésitez pas à signaler les BUGS !!
